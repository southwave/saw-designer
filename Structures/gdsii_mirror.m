classdef gdsii_mirror<handle
    %The coordinate of canvas center is (0,0)
    properties
        version=2.0
        %必须输入的参数
        name;
        NG;
        mirror_period;%um
        mirror_finger_length;%um
        %可选修改的参数
        type=1;%1短路或者2开路，甚至更复杂的结构
        mirror_finger_width;%um
        bus_bar_width=50;%um
        bus_bar_length;%um
%         xy;
        %计算的结构参数
        min_x;
        max_x;
        min_y;
        max_y;
        length;
        width;
        %生成的structure
        layer5=5;
        structure;
        top;
    end
    
    methods
        function mirror=gdsii_mirror(layout)
            %initialization
            for field=string(fieldnames(layout))'
                if isprop(mirror,field)
                    mirror.(field)=layout.(field);
                end
            end
            mirror.check_layout()
            if isempty(mirror.mirror_finger_width)
                mirror.mirror_finger_width=mirror.mirror_period/2;
            end
            %可选修改的参数
            mirror.bus_bar_length=(2*mirror.NG-1)*mirror.mirror_period/2;
            mirror.gen_structure()
        end
        
        function check_layout(mirror)
            flag=~isempty(mirror.name)&&~isempty(mirror.NG)&&...
                ~isempty(mirror.mirror_period)&&~isempty(mirror.mirror_finger_length);
            if ~flag
                error('请假查mirror的name，NG，mirror_period和mirror_finger_length是否都已输入')
            end
            
        end
        
        function gen_structure(mirror)
            % create a structure containing a boundary element describing a grating line
            switch mirror.type
                case {1,2}
                    if mirror.NG>1&&mod(mirror.NG,1)==0
                        %% finger
                        xy = [0,0;
                            mirror.mirror_finger_width,0;
                            mirror.mirror_finger_width,mirror.mirror_finger_length;
                            0,mirror.mirror_finger_length;
                            0,0]-[mirror.mirror_finger_width/2,mirror.mirror_finger_length/2];
                        finger1 = gds_structure([mirror.name,'_Single_Finger'], gds_element('boundary', 'xy',xy, 'layer',mirror.layer5));
                        % replicate the structure to make a grating
                        grating_width=mirror.NG*mirror.mirror_period;
                        arc = [0,0;
                            grating_width,0;
                            0,mirror.mirror_finger_length]-[mirror.mirror_period/2*(mirror.NG-1),0];
                        adim.row = 1;
                        adim.col = mirror.NG;
                        repe1 = gds_element('aref', 'sname',strcat(mirror.name,'_Single_Finger'), 'xy',arc, 'adim',adim);
                        fingers1 = gds_structure([mirror.name,'_Fingers_Array'], repe1);
                        mirror.structure = {finger1, fingers1};
                        
                        %计算min_x, max_x, min_y, max_y
                        mirror.min_x=arc(1,1)-mirror.mirror_finger_width/2;
                        mirror.max_x=mirror.min_x+mirror.bus_bar_length;
                        mirror.min_y=-mirror.mirror_finger_length/2;
                        mirror.max_y=mirror.mirror_finger_length/2;
                        if mirror.type==1%短路栅需要汇流条
                            %bus bar lower
                            xy=-[mirror.mirror_period/2*(mirror.NG-1),0]+...
                                [-mirror.mirror_finger_width/2,-mirror.mirror_finger_length/2-mirror.bus_bar_width]+...
                                [0,0;
                                0,mirror.bus_bar_width;
                                mirror.bus_bar_length,mirror.bus_bar_width;
                                mirror.bus_bar_length,0;
                                0,0];
                            bus_bar_lower=gds_element('boundary','xy',xy,'layer',mirror.layer5);
                            
                            %计算min_y
                            mirror.min_y=xy(1,2);
                            
                            %bus bar upper
                            xy=[-mirror.mirror_period/2*(mirror.NG-1),0]+...
                                [-mirror.mirror_finger_width/2,mirror.mirror_finger_length/2]+...
                                [0,0;
                                0,mirror.bus_bar_width;
                                mirror.bus_bar_length,mirror.bus_bar_width;
                                mirror.bus_bar_length,0;
                                0,0];
                            bus_bar_upper=gds_element('boundary','xy',xy,'layer',mirror.layer5);
                            bus_bar_cell=gds_structure(strcat(mirror.name,'_Bus_Bar'),{bus_bar_lower,bus_bar_upper});
                            %再把bus_bar_cell封装进mirror structure里面
                            mirror.structure{end+1}=bus_bar_cell;
                            
                            %max_y
                            mirror.max_y=xy(2,2);
                        end
                    else
                        error('mirror电极数应大于1的整数')
                    end
                otherwise
                    %todo
            end
            mirror.length=mirror.max_x-mirror.min_x;
            mirror.width=mirror.max_y-mirror.min_y;
            %添加到top
            mirror.top_structure()
        end
        
        function top_structure(mirror)
            mirror.top = gds_structure(strcat(mirror.name,'_TOP'));
            for ii=1:length(mirror.structure)
                if ii~=1
                    mirror.top = add_ref(mirror.top, mirror.structure{ii}, 'xy',[0,0]); % this is a bit ugly ...
                end
            end
        end
        
        function show_layout(mirror,shift,ax)
            if nargin<3
                ax=gca;
            end
            if nargin<2
                shift=0;
            end
            line=plot(ax,[mirror.min_x,mirror.min_x,mirror.max_x,mirror.max_x,mirror.min_x]+shift(1),[mirror.min_y,mirror.max_y,mirror.max_y,mirror.min_y,mirror.min_y]+shift(2));
            text(ax,(mirror.min_x+mirror.max_x)/2+shift(1),(mirror.min_y+mirror.max_y)/2+shift(2),mirror.name,'interpreter','none','color',line.Color,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
        end
    end
    methods(Static)
        
    end
end