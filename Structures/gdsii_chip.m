classdef gdsii_chip<handle
    properties
        version=3.0;
        %必须输入的参数
        chip_name='Default_Chip_Name';
        folder;
        %可修改的参数
        writable_names=["version","x","y","device_xy","chip_name","group_name","folder",insertAfter(repmat("layer",1,6),'r',string(1:6))];
        x=0;
        y=0;
        device_xy;
        layer1=1;
        layer2=2;
        layer3=3;
        layer4=4;
        layer5=5;
        layer6=6;
        group_name='USTC-QT-Group';
        chip_layout;
        %计算得到的结构参数
        devices;
        min_x=0;
        min_y=0;
        max_x=0;
        max_y=0;
        mirror_distance=0;
        idt_mirror_distance=0;
        text_width;
        text_value;
        %结构
        structure;
        top;
        glib;
        %单位
        uunit=1e-6;
    end
    
    methods
        function chip=gdsii_chip(chip_parameters,chip_layout)
            %initialization
            %必须输入的参数
            if ~isempty(chip_parameters)
                for name=string(fieldnames(chip_parameters))'
                    if any(strcmp(chip.writable_names,name))
                        chip.(name)=chip_parameters.(name);
                    end
                end
            end
            chip.chip_layout=chip_layout;
            chip.device_xy=chip.device_xy+[chip.x,chip.y];
        end
        
        function gen_structure(chip)
            chip.top = gds_structure(char(strcat(chip.chip_name,'_chip_TOP')));
            for ii=1:length(chip.chip_layout.layout)
                chip.chip_layout.parameters{ii}.name=strcat(chip.chip_name,'_',chip.chip_layout.parameters{ii}.name);
                chip.structure{ii}=gdsii_SAW(chip.chip_layout.parameters{ii},chip.chip_layout.layout{ii});
                %把所有top结构封装进chip.top这个顶层cell中
                chip.top = add_ref(chip.top, chip.structure{ii}.top, 'xy',chip.device_xy(ii,:)); % device_top
            end
            %计算min_x, max_x, min_y, max_y
            chip.min_x=min(arrayfun(@(x) chip.structure{x}.min_x+min(chip.device_xy(:,1)),1:length(chip.structure)));
            chip.max_x=max(arrayfun(@(x) chip.structure{x}.max_x+max(chip.device_xy(:,1)),1:length(chip.structure)));
            chip.min_y=min(arrayfun(@(x) chip.structure{x}.min_y+min(chip.device_xy(:,2)),1:length(chip.structure)));
            chip.max_y=max(arrayfun(@(x) chip.structure{x}.max_y+max(chip.device_xy(:,2)),1:length(chip.structure)));
        end
        
        function gen_library(chip)
            %把chip.top加入到library中
            chip.glib = gds_library(chip.chip_name, 'uunit',chip.uunit, 'dbunit',1e-9,chip.top);
            %把其他的cell加入到library中
            for ii=1:length(chip.structure)%device loop
                if ~isempty(chip.structure{ii}.structure)
                    %structure{ii}.top为device_top
                    %structure{ii}.structure.top为构件top
                    chip.glib = add_struct(chip.glib,chip.structure{ii}.top);
                    %chip.structure{ii}.structure.structure为构件内部的cell
                    for jj=1:length(chip.structure{ii}.structure)%building block loop
                        if ~isempty(chip.structure{ii}.structure)
                            chip.glib = add_struct(chip.glib,chip.structure{ii}.structure{jj}.top);
                            for kk=1:length(chip.structure{ii}.structure{jj}.structure)%internal loop
                                if ~isempty(chip.structure{ii}.structure{jj}.structure)
                                    chip.glib = add_struct(chip.glib,chip.structure{ii}.structure{jj}.structure{kk});
                                end
                            end
                        end
                    end
                end
            end
        end
        
        function write_library(chip)
            write_gds_library(chip.glib, char(strcat('!',fullfile(chip.folder,chip.chip_name),'.gds')));
        end
        
        function write_parameters(chip,filename)
            if nargin<2
                filename=fullfile(chip.folder,strcat(chip.chip_name,'-parameters.txt'));
            end
            fid=fopen(filename, 'wt');
            fprintf(fid,'version=%s\n',string(chip.version));
            fprintf(fid,'group_name=%s\n',chip.group_name);
            fprintf(fid,'date=%s\n',char(datetime('now','TimeZone','local','Format','y-MM-d HH:mm:ss')));
            fprintf(fid,'unit=%s m\n',string(chip.uunit));
            fprintf(fid,'chip_name=%s\n',chip.chip_name);
            fprintf(fid,'\n%%----Chip Structure----\n');
            fprintf(fid,'center point=%s,%s\n',string([chip.x,chip.y]));
            fprintf(fid,'chip length=%s\n',string(chip.max_x-chip.min_x));
            fprintf(fid,'chip width=%s\n',string(chip.max_y-chip.min_y));
            fclose(fid);
            for ii=1:length(chip.structure)
                chip.structure{ii}.write_parameters(filename);
            end
        end
        
        function show_layout(chip,ax)
            if nargin<2
                ax=gca;
            end
            line=plot(ax,[chip.min_x,chip.min_x,chip.max_x,chip.max_x,chip.min_x],[chip.min_y,chip.max_y,chip.max_y,chip.min_y,chip.min_y]);
            text(ax,(chip.min_x+chip.max_x)/2,(chip.min_y+chip.max_y)/2,chip.chip_name,'interpreter','none','color',line.Color,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
            hold(ax,'on')
            title(ax,'Structure Preview')
            xlabel(ax,'x (\mu m)')
            ylabel(ax,'y (\mu m)')
            arrayfun(@(x) chip.structure{x}.show_layout(chip.device_xy(x,:),ax), 1:length(chip.structure))
%             line=plot(ax,(chip.min_x+chip.max_x)/2+[-chip.text_width/2,-chip.text_width/2,chip.text_width/2,chip.text_width/2],[chip.min_y,chip.min_y+chip.font_height,chip.min_y+chip.font_height,chip.min_y]);
%             text(ax,(chip.min_x+chip.max_x)/2,chip.min_y+chip.font_height/2,chip.text_value,'interpreter','none','color',line.Color,...
%                 'HorizontalAlignment','center','VerticalAlignment','middle')
        end
        
    end
    methods(Static)
        
    end
end