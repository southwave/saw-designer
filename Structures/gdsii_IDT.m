classdef gdsii_IDT<handle
    %The coordinate of canvas center is (0,0)
    properties
        version=2.0
        %必须输入的参数
        name;
        type=1;%单叉指或者双叉指，甚至更复杂的结构
        NI;
        aperture;%um
        lambda;%um
        %可选修改的参数
        idt_finger_width;%默认按照金属化率为0.5来计算电极宽度
        idt_finger_length;%um
        idt_margin;%um
        bus_bar_width;%um
        bus_bar_length;%um
        pad_line_width_upper=50;%um
        pad_line_width_lower=50;%um
        pad_line_length_upper=200;%um
        pad_line_length_lower=200;%um
        pad_size_upper=200;%um
        pad_size_lower=200;%um
        pad_funnel_length=200;%um
%         xy;
        %计算得到的结构参数
        min_x;
        max_x;
        min_y;
        max_y;
        length;
        width;
        %生成的structure
        layer1=1;
        layer2=2;
        layer3=3;
        layer4=4;
        structure;
        top;
    end
    
    methods
        function IDT=gdsii_IDT(layout)
            %initialization
            for field=string(fieldnames(layout))'
                if isprop(IDT,field)
                    IDT.(field)=layout.(field);
                end
            end
            IDT.check_layout()
            if isempty(IDT.idt_finger_width)
                switch IDT.type
                    case 1
                        IDT.idt_finger_width=IDT.lambda/4;
                    case 2
                        IDT.idt_finger_width=IDT.lambda/8;
                    otherwise
                        IDT.idt_finger_width=IDT.lambda/4;
                end
            end
            %可选修改的参数
            if isempty(IDT.idt_margin)
                IDT.idt_margin=IDT.lambda;
            end
            IDT.idt_finger_length=IDT.aperture+IDT.idt_margin;
            if isempty(IDT.bus_bar_width)
                IDT.bus_bar_width=IDT.lambda;
            end
            if isempty(IDT.bus_bar_length)
                switch IDT.type
                    case 1
                        IDT.bus_bar_length=(2*IDT.NI-1)*IDT.lambda/4;
                    case 2
                        IDT.bus_bar_length=(2*IDT.NI-1)*IDT.lambda/8;
                    otherwise
                        IDT.bus_bar_length=(2*IDT.NI-1)*IDT.lambda/4;
                end
            end
            IDT.gen_structure()
        end
        
        function check_layout(IDT)
            %检查必须输入的参数是否已经输入了
            switch IDT.type
                case {1,2}
                    flag=~isempty(IDT.name)&&~isempty(IDT.NI)&&...
                        ~isempty(IDT.aperture)&&~isempty(IDT.lambda);
                    if ~flag
                        error('IDT输入参数缺失，请检查name，NI，aperture和lambda是否都已输入')
                    end
                otherwise
            end
        end
        
        function gen_structure(IDT)
            % create a structure containing a boundary element describing a grating line
            switch IDT.type
                case 1
                    if IDT.NI>1&&mod(IDT.NI,1)==0
                        %% finger lower
                        xy = [0,0;
                            IDT.idt_finger_width,0;
                            IDT.idt_finger_width,IDT.idt_finger_length;
                            0,IDT.idt_finger_length;
                            0,0]-[IDT.idt_finger_width/2,IDT.idt_finger_length/2];
                        finger1 = gds_structure(strcat(IDT.name,'_Lower_Line'), gds_element('boundary', 'xy',xy, 'layer',IDT.layer1));
                        % replicate the structure to make a grating
                        grating_width=ceil(IDT.NI/2)*IDT.lambda;
                        arc = [0,0;
                            grating_width,0;
                            0,IDT.idt_finger_length]-[IDT.lambda/4*(IDT.NI-1),IDT.idt_margin/2];%以左下角点为基准
                        adim.row = 1;
                        adim.col = ceil(IDT.NI/2);
                        repe1 = gds_element('aref', 'sname',strcat(IDT.name,'_Lower_Line'), 'xy',arc, 'adim',adim);
                        fingers1 = gds_structure(strcat(IDT.name,'_Lower_Array'), repe1);
                        %% finger upper
                        xy = [0,0;
                            IDT.idt_finger_width,0;
                            IDT.idt_finger_width,IDT.idt_finger_length;
                            0,IDT.idt_finger_length;
                            0,0]-[IDT.idt_finger_width/2,IDT.idt_finger_length/2];
                        finger2 = gds_structure(strcat(IDT.name,'_Upper_Line'), gds_element('boundary', 'xy',xy, 'layer',IDT.layer2));
                        % replicate the structure to make a grating
                        grating_width=floor(IDT.NI/2)*IDT.lambda;
                        arc = [0,0;
                            grating_width,0;
                            0,IDT.idt_finger_length]-[IDT.lambda/4*(IDT.NI-1)-IDT.lambda/2,-IDT.idt_margin/2];%以左下角点为基准
                        adim.row = 1;
                        adim.col = floor(IDT.NI/2);
                        repe2 = gds_element('aref', 'sname',strcat(IDT.name,'_Upper_Line'), 'xy',arc, 'adim',adim);
                        fingers2 = gds_structure(strcat(IDT.name,'_Upper_Array'), repe2);
                        IDT.structure = {finger1, fingers1,finger2, fingers2};
                        
                        %% pad lower
                        
                        %bus bar
                        xy=[-IDT.lambda/4*(IDT.NI-1),-IDT.idt_margin/2]+...
                            [-IDT.idt_finger_width/2,-IDT.idt_finger_length/2-IDT.bus_bar_width]+...
                            [0,0;
                            0,IDT.bus_bar_width;
                            IDT.bus_bar_length,IDT.bus_bar_width;
                            IDT.bus_bar_length,0;
                            0,0];
                        bus_bar_lower=gds_element('boundary','xy',xy,'layer',IDT.layer1);
                        
                        %计算min_x
                        IDT.min_x=xy(1,1);
                        
                        %pad line
                        xy=xy(1,:)+[(IDT.bus_bar_length-IDT.pad_line_width_lower)/2,-IDT.pad_line_length_lower]+...
                            [0,0;
                            0,IDT.pad_line_length_lower+min(IDT.bus_bar_width/5,5);%向上挪bus bar宽度的1/5防止电极断开
                            IDT.pad_line_width_lower,IDT.pad_line_length_lower+min(IDT.bus_bar_width/5,5);%向上挪bus bar宽度的1/5防止电极断开
                            IDT.pad_line_width_lower,0;
                            0,0];
                        pad_line_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %pad funnel
                        xy=xy(1,:)+...
                            [0,0;
                            IDT.pad_line_width_lower,0;
                            (IDT.pad_size_lower+IDT.pad_line_width_lower)/2,-IDT.pad_funnel_length;
                            (-IDT.pad_size_lower+IDT.pad_line_width_lower)/2,-IDT.pad_funnel_length;
                            0,0];
                        pad_funnel_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %pad
                        xy=xy(4,:)+...
                            [0,-IDT.pad_size_lower]+...
                            [0,0;
                            0,IDT.pad_size_lower;
                            IDT.pad_size_lower,IDT.pad_size_lower;
                            IDT.pad_size_lower,0;
                            0,0];
                        pad_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %将pad封装进structure里面
                        pad_cell_lower=gds_structure(strcat(IDT.name,'_Pad_Lower'),{bus_bar_lower,pad_line_lower,pad_funnel_lower,pad_lower});
                        %再把pad cell封装进IDT structure里面
                        IDT.structure{end+1}=pad_cell_lower;
                        
                        %计算min_y
                        IDT.min_y=xy(1,2);
                        %% pad upper
                        
                        %bus bar
                        xy=-[IDT.lambda/4*(IDT.NI-1),IDT.idt_margin/2]+...
                            [-IDT.idt_finger_width/2,IDT.idt_finger_length/2+IDT.idt_margin]+...
                            [0,0;
                            0,IDT.bus_bar_width;
                            IDT.bus_bar_length,IDT.bus_bar_width;
                            IDT.bus_bar_length,0;
                            0,0];
                        bus_bar_upper=gds_element('boundary','xy',xy,'layer',IDT.layer2);
                        
                        %计算max_x
                        IDT.max_x=xy(3,1);
                        
                        %pad line
                        xy=xy(1,:)+[IDT.bus_bar_length/2-IDT.pad_line_width_upper/2,IDT.bus_bar_width]+...
                            [0,0-min(IDT.bus_bar_width/5,5);%向下挪bus bar宽度的1/5防止电极断开
                            0,IDT.pad_line_length_upper;
                            IDT.pad_line_width_upper,IDT.pad_line_length_upper;
                            IDT.pad_line_width_upper,0-min(IDT.bus_bar_width/5,5);%向下挪bus bar宽度的1/5防止电极断开
                            0,0-min(IDT.bus_bar_width/5,5)];%向下挪bus bar宽度的1/5防止电极断开
                        pad_line_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        %pad funnel
                        xy=xy(2,:)+...
                            [0,0;
                            -(IDT.pad_size_upper-IDT.pad_line_width_upper)/2,IDT.pad_funnel_length;
                            (IDT.pad_size_upper+IDT.pad_line_width_upper)/2,IDT.pad_funnel_length;
                            IDT.pad_line_width_upper,0;
                            0,0];
                        pad_funnel_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        %pad
                        xy=xy(2,:)+...
                            [0,0;
                            0,IDT.pad_size_upper;
                            IDT.pad_size_upper,IDT.pad_size_upper;
                            IDT.pad_size_upper,0;
                            0,0];
                        pad_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        
                        %将pad封装进structure里面
                        pad_cell_upper=gds_structure(strcat(IDT.name,'_Pad_Upper'),{bus_bar_upper,pad_line_upper,pad_funnel_upper,pad_upper});
                        %再把pad cell封装进IDT structure里面
                        IDT.structure{end+1}=pad_cell_upper;
                        
                        %计算max_y
                        IDT.max_y=xy(3,2);
                    else
                        error('IDT电极数应大于1的整数')
                    end
                case 2
                    if mod(IDT.NI-1,2)==1&&IDT.NI>2
                        %% finger lower
                        xy = [0,0;
                            IDT.idt_finger_width,0;
                            IDT.idt_finger_width,IDT.idt_finger_length;
                            0,IDT.idt_finger_length;
                            0,0]-[IDT.idt_finger_width/2,IDT.idt_finger_length/2];
                        finger1 = gds_structure(strcat(IDT.name,'_Lower_Line'),...
                            {gds_element('boundary', 'xy',xy-[IDT.lambda/8,0], 'layer',IDT.layer1),gds_element('boundary','xy',xy+[IDT.lambda/8,0],'layer',IDT.layer1)});
                        % replicate the structure to make a grating
                        grating_width=ceil(IDT.NI/4)*IDT.lambda;
                        arc = [0,0;
                            grating_width,0;
                            0,IDT.idt_finger_length]-[IDT.lambda/4*(IDT.NI/2-1),IDT.idt_margin/2];
                        adim.row = 1;
                        adim.col = ceil(IDT.NI/4);
                        repe1 = gds_element('aref', 'sname',strcat(IDT.name,'_Lower_Line'), 'xy',arc, 'adim',adim);
                        fingers1 = gds_structure(strcat(IDT.name,'_Lower_Array'), repe1);
                        
                        %% finger upper
                        xy = [0,0;
                            IDT.idt_finger_width,0;
                            IDT.idt_finger_width,IDT.idt_finger_length;
                            0,IDT.idt_finger_length;
                            0,0]-[IDT.idt_finger_width/2,IDT.idt_finger_length/2];
                        finger2 = gds_structure(strcat(IDT.name,'_Upper_Line'),...
                            {gds_element('boundary', 'xy',xy-[IDT.lambda/8,0], 'layer',IDT.layer2),gds_element('boundary','xy',xy+[IDT.lambda/8,0],'layer',IDT.layer2)});
                        % replicate the structure to make a grating
                        grating_width=floor(IDT.NI/4)*IDT.lambda;
                        arc = [0,0;
                            grating_width,0;
                            0,IDT.idt_finger_length]-[IDT.lambda/4*(IDT.NI/2-1)-IDT.lambda/2,-IDT.idt_margin/2];
                        adim.row = 1;
                        adim.col = floor(IDT.NI/4);
                        repe2 = gds_element('aref', 'sname',strcat(IDT.name,'_Upper_Line'), 'xy',arc, 'adim',adim);
                        fingers2 = gds_structure(strcat(IDT.name,'_Upper_Array'), repe2);
                        IDT.structure = {finger1, fingers1,finger2, fingers2};
                        
                        %% pad lower
                        
                        %bus bar
                        xy=[-IDT.lambda/4*(IDT.NI/2-1),-IDT.idt_margin/2]+...
                            [-IDT.idt_finger_width*3/2,-IDT.idt_finger_length/2-IDT.bus_bar_width]+...
                            [0,0;
                            0,IDT.bus_bar_width;
                            IDT.bus_bar_length,IDT.bus_bar_width;
                            IDT.bus_bar_length,0;
                            0,0];
                        bus_bar_lower=gds_element('boundary','xy',xy,'layer',IDT.layer1);
                        
                        %计算min_x
                        IDT.min_x=xy(1,1);
                        
                        %pad line
                        xy=xy(1,:)+[(IDT.bus_bar_length-IDT.pad_line_width_lower)/2,-IDT.pad_line_length_lower]+...
                            [0,0;
                            0,IDT.pad_line_length_lower+min(IDT.bus_bar_width/5,5);
                            IDT.pad_line_width_lower,IDT.pad_line_length_lower+min(IDT.bus_bar_width/5,5);
                            IDT.pad_line_width_lower,0;
                            0,0];
                        pad_line_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %pad funnel
                        xy=xy(1,:)+...
                            [0,0;
                            IDT.pad_line_width_lower,0;
                            (IDT.pad_size_lower+IDT.pad_line_width_lower)/2,-IDT.pad_funnel_length;
                            (-IDT.pad_size_lower+IDT.pad_line_width_lower)/2,-IDT.pad_funnel_length;
                            0,0];
                        pad_funnel_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %pad
                        xy=xy(4,:)+...
                            [0,-IDT.pad_size_lower]+...
                            [0,0;
                            0,IDT.pad_size_lower;
                            IDT.pad_size_lower,IDT.pad_size_lower;
                            IDT.pad_size_lower,0;
                            0,0];
                        pad_lower=gds_element('boundary','xy',xy,'layer',IDT.layer3);
                        %将pad封装进structure里面
                        pad_cell_lower=gds_structure(strcat(IDT.name,'_Pad_Lower'),{bus_bar_lower,pad_line_lower,pad_funnel_lower,pad_lower});
                        %再把pad cell封装进IDT structure里面
                        IDT.structure{end+1}=pad_cell_lower;
                        
                        %计算min_y
                        IDT.min_y=xy(1,2);
                        %% pad upper
                        
                        %bus bar
                        xy=-[IDT.lambda/4*(IDT.NI/2-1),IDT.idt_margin/2]+...
                            [-IDT.idt_finger_width*3/2,IDT.idt_finger_length/2+IDT.idt_margin]+...
                            [0,0;
                            0,IDT.bus_bar_width;
                            IDT.bus_bar_length,IDT.bus_bar_width;
                            IDT.bus_bar_length,0;
                            0,0];
                        bus_bar_upper=gds_element('boundary','xy',xy,'layer',IDT.layer2);
                        
                        %计算max_x
                        IDT.max_x=xy(3,1);
                        
                        %pad line
                        xy=xy(1,:)+[IDT.bus_bar_length/2-IDT.pad_line_width_upper/2,IDT.bus_bar_width]+...
                            [0,0-min(IDT.bus_bar_width/5,5);
                            0,IDT.pad_line_length_upper;
                            IDT.pad_line_width_upper,IDT.pad_line_length_upper;
                            IDT.pad_line_width_upper,0-min(IDT.bus_bar_width/5,5);
                            0,0-min(IDT.bus_bar_width/5,5)];
                        pad_line_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        %pad funnel
                        xy=xy(2,:)+...
                            [0,0;
                            -(IDT.pad_size_upper-IDT.pad_line_width_upper)/2,IDT.pad_funnel_length;
                            (IDT.pad_size_upper+IDT.pad_line_width_upper)/2,IDT.pad_funnel_length;
                            IDT.pad_line_width_upper,0;
                            0,0];
                        pad_funnel_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        %pad
                        xy=xy(2,:)+...
                            [0,0;
                            0,IDT.pad_size_upper;
                            IDT.pad_size_upper,IDT.pad_size_upper;
                            IDT.pad_size_upper,0;
                            0,0];
                        pad_upper=gds_element('boundary','xy',xy,'layer',IDT.layer4);
                        
                        %将pad封装进structure里面
                        pad_cell_upper=gds_structure(strcat(IDT.name,'_Pad_Upper'),{bus_bar_upper,pad_line_upper,pad_funnel_upper,pad_upper});
                        %再把pad cell封装进IDT structure里面
                        IDT.structure{end+1}=pad_cell_upper;
                        
                        %计算max_y
                        IDT.max_y=xy(3,2);
                    else
                        error('双叉指IDT电极数应为2的整数倍，且不能为2')
                    end
                otherwise
            end
            IDT.length=IDT.max_x-IDT.min_x;
            IDT.width=IDT.max_y-IDT.min_y;
            %添加到top
            IDT.top_structure()
        end
        
        function top_structure(IDT)
            IDT.top = gds_structure(strcat(IDT.name,'_TOP'));
            IDT.top = add_ref(IDT.top, IDT.structure{2}, 'xy',[0,0]); % this is a bit ugly ...
            IDT.top = add_ref(IDT.top, IDT.structure{4}, 'xy',[0,0]); % this is a bit ugly ...
            IDT.top = add_ref(IDT.top, IDT.structure{5}, 'xy',[0,0]); % this is a bit ugly ...
            IDT.top = add_ref(IDT.top, IDT.structure{6}, 'xy',[0,0]); % this is a bit ugly ...
        end
        
        function show_layout(IDT,shift,ax)
            if nargin<3
                ax=gca;
            end
            if nargin<2
                shift=0;
            end
            line=plot(ax,[IDT.min_x,IDT.min_x,IDT.max_x,IDT.max_x,IDT.min_x]+shift(1),[IDT.min_y,IDT.max_y,IDT.max_y,IDT.min_y,IDT.min_y]+shift(2));
            text(ax,(IDT.min_x+IDT.max_x)/2+shift(1),(IDT.min_y+IDT.max_y)/2+shift(2),IDT.name,'interpreter','none','color',line.Color,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
        end
    end
    
    methods(Static)
        
    end
end