classdef gdsii_SAW<handle
    %gdsii_SAW(parameters,layout)
    %输入SAW器件的名字name，文件存储位置folder，器件结构变量layout
    %程序将自动在folder下生成name.gds文件和name-parameters.txt说明文件
    %
    %layout示例1
    %生成一个名为IDT_Left的IDT，lambda为14 mu m，孔径为200 lambda
    %IDT类型为双叉指，电极数为22根，IDT中心在画布的位置为[-400 lambda,0]
    %layout(1).name='IDT_Left';
    %layout(1).stype='IDT';%IDT or mirror
    %layout(1).layout.lambda=14;
    %layout(1).layout.aperture=200*layout(1).layout.lambda;
    %layout(1).layout.NI=11*2;
    %layout(1).layout.type=2;%双叉指
    %layout(1).xy=[-400*layout(1).layout.lambda,0];
    %
    %layout示例2
    %生成一个名为mirror_Left的mirror，两根电极的间距为7 mu m的短路mirror
    %电极长度为2倍间距的200倍，电极宽度为电极周期的一半
    %电极根数为200，mirror的中心位置为2倍电极周期的[-400,0]
    %layout(2).name='mirror_Left';
    %layout(2).stype='mirror';
    %layout(2).layout.type=1;%短路
    %layout(2).layout.mirror_period=7;
    %layout(2).layout.mirror_finger_length=200*2*layout(2).layout.mirror_period;
    %layout(2).layout.NG=200;
    %layout(2).xy=[-400*layout(2).layout.mirror_period*2,0];
    properties
        version=3.0;
        %必须输入的参数
        name='Default_Name';
        folder;
        layout;
        %可修改的参数
        writable_names=["version","x","y","name","group_name","font_height","folder",insertAfter(repmat("layer",1,6),'r',string(1:6))];
        x=0;
        y=0;
        layer1=1;
        layer2=2;
        layer3=3;
        layer4=4;
        layer5=5;
        layer6=6;
        group_name='USTC-QT-Group';
        font_height=100;
        %计算得到的结构参数
        min_x=0;
        min_y=0;
        max_x=0;
        max_y=0;
        mirror_distance=0;
        idt_mirror_distance=0;
        idt_distance=0;
        NI=0;
        NG=0;
        text_width;
        text_value;
        %结构
        structure;
        top;
        glib;
        %单位
        uunit=1e-6;
    end
    
    methods
        function SAW=gdsii_SAW(parameters,layout)
            %initialization
            %必须输入的参数
            if ~isempty(parameters)
                for name=string(fieldnames(parameters))'
                    if any(strcmp(SAW.writable_names,name))
                        SAW.(name)=parameters.(name);
                    end
                end
            end
            for ii=1:length(layout)
                layout(ii).name=strcat(char(SAW.name),'_',char(layout(ii).name));
            end
            layout(end+1).stype='text';
            SAW.layout=layout;
            
            SAW.gen_structure()
%             SAW.gen_library()
%             SAW.write_library()
%             SAW.write_parameters()
        end
        
        function gen_structure(SAW)
            idt_count=0;
            mirror_count=0;
            SAW.structure=[];
            for ii=1:length(SAW.layout)
                switch lower(SAW.layout(ii).stype)
                    case 'idt'
                        idt_count=idt_count+1;
                        SAW.layout(ii).layout.name=strcat(SAW.layout(ii).name,'_IDT',num2str(idt_count));
                        for jj=1:4
                            SAW.layout(ii).layout.(['layer',num2str(jj)])=SAW.(['layer',num2str(jj)]);
                        end
                        SAW.structure{ii}=gdsii_IDT(SAW.layout(ii).layout);
                    case 'mirror'
                        mirror_count=mirror_count+1;
                        SAW.layout(ii).layout.name=[SAW.layout(ii).name,'_mirror',num2str(mirror_count)];
                        SAW.layout(ii).layout.layer5=SAW.layer5;
                        SAW.structure{ii}=gdsii_mirror(SAW.layout(ii).layout);
                    case 'text'
                        jj=ii;
                        SAW.layout(ii).layout.name=SAW.name;
                        SAW.layout(ii).layout.group_name=SAW.group_name;
                        SAW.layout(ii).layout.font_height=SAW.font_height;
                        SAW.layout(ii).layout.layer6=SAW.layer6;
                        SAW.layout(ii).xy=[0,0];
                        SAW.structure{ii}=gdsii_text(SAW.layout(ii).layout);
                end
                SAW.layout(ii).xy=SAW.layout(ii).xy+[SAW.x,SAW.y];
            end
            
            %计算min_x, max_x, min_y, max_y
            SAW.min_x=min(arrayfun(@(x) SAW.structure{x}.min_x+SAW.layout(x).xy(1),1:length(SAW.structure)));
            SAW.max_x=max(arrayfun(@(x) SAW.structure{x}.max_x+SAW.layout(x).xy(1),1:length(SAW.structure)));
            SAW.min_y=min(arrayfun(@(x) SAW.structure{x}.min_y+SAW.layout(x).xy(2),1:length(SAW.structure)));
            SAW.max_y=max(arrayfun(@(x) SAW.structure{x}.max_y+SAW.layout(x).xy(2),1:length(SAW.structure)));
            
            SAW.layout(jj).xy=[(SAW.min_x+SAW.max_x)/2-SAW.structure{jj}.text_width/2,...
                SAW.min_y-SAW.font_height*5];
            SAW.min_x=min(SAW.min_x,SAW.layout(jj).xy(1));
            SAW.max_x=max(SAW.max_x,SAW.layout(jj).xy(1));
            SAW.min_y=SAW.min_y-SAW.font_height*5;
            
            % 生成器件的TOP层
            SAW.top = gds_structure(char(strcat(SAW.name,'_device_TOP')));
            for ii=1:length(SAW.structure)
                %把所有top结构封装进SAW.top这个顶层cell中
                SAW.top = add_ref(SAW.top, SAW.structure{ii}.top, 'xy',SAW.layout(ii).xy); % this is a bit ugly ...
            end
        end
        
        function gen_library(SAW)
            
            %把SAW.top加入到library中
            SAW.glib = gds_library(SAW.name, 'uunit',SAW.uunit, 'dbunit',1e-9,SAW.top);
            %把其他的cell加入到library中
            for ii=1:length(SAW.structure)
                if ~isempty(SAW.structure{ii}.structure)
                    SAW.glib = add_struct(SAW.glib,SAW.structure{ii}.structure,SAW.structure{ii}.top);
                else
                    SAW.glib = add_struct(SAW.glib,SAW.structure{ii}.top);
                end
            end
        end
        
        function write_library(SAW)
            write_gds_library(SAW.glib, char(strcat('!',fullfile(SAW.folder,SAW.name),'.gds')));
        end
        
        function write_parameters(SAW,filename)
            if nargin<2
                filename=fullfile(SAW.folder,strcat(SAW.name,'-parameters.txt'));
            end
            fid=fopen(filename, 'at');
            fprintf(fid,'\n%%----Device Structure----\n');
            fprintf(fid,'version=%s\n',string(SAW.version));
            fprintf(fid,'group_name=%s\n',SAW.group_name);
            fprintf(fid,'date=%s\n',char(datetime('now','TimeZone','local','Format','y-MM-d HH:mm:ss')));
            fprintf(fid,'unit=%s m\n',string(SAW.uunit));
            fprintf(fid,'device_name=%s\n',SAW.name);
            fprintf(fid,'center point=%s,%s\n',string([SAW.x,SAW.y]));
            fprintf(fid,'device length=%s\n',string(SAW.max_x-SAW.min_x));
            fprintf(fid,'device width=%s\n',string(SAW.max_y-SAW.min_y));
            %目前这个版本的顶层设计比较混杂，各个构件的位置存在SAW这一层，构件本身不知道自己在什么位置
            for ii=1:length(SAW.structure)
                fprintf(fid,'\n%%-----structure%s----\n',num2str(ii));
                for name=string(fieldnames(SAW.structure{ii}))'
                    if ~matches(name,["structure","top"])
                        fprintf(fid,'%s=%s\n',[name,join(string(SAW.structure{ii}.(name)),',')]);
                    end
                end
                fprintf(fid,'xy=%s\n',join(string(SAW.layout(ii).xy),','));
            end
            %SAW.layout是输入的参数，参数文件应该记录计算好的输出参数
%             for ii=1:length(SAW.layout)
%                 fprintf(fid,'\n%%-----layout%s----\n',num2str(ii));
%                 field_names=string(fieldnames(SAW.layout(ii)));
%                 for jj=1:length(field_names)
%                     if ~isempty(SAW.layout(ii).(field_names(jj)))&&~strcmp(field_names(jj),'layout')
%                         fprintf(fid,'layout%s_%s=%s\n',[string(num2str(ii)),string(field_names(jj)),strcat(string(SAW.layout(ii).(field_names(jj))),',')]);
%                     elseif strcmp(field_names(jj),'layout')
%                         building_block_field_names=string(fieldnames(SAW.layout(ii).(field_names(jj))));
%                         for kk=1:length(building_block_field_names)
%                             fprintf(fid,'layout%s_%s=%s\n',[string(num2str(ii)),string(building_block_field_names(kk)),...
%                                 strcat(string(SAW.layout(ii).(field_names(jj)).(building_block_field_names(kk))),',')]);
%                         end
%                     end
%                 end
%             end
            fclose(fid);
        end
        
        function show_layout(SAW,shift,ax)
            if nargin<3
                ax=gca;
            end
            if nargin<2
                shift=0;
            end
            %画SAW器件范围框
            line=plot(ax,[SAW.min_x,SAW.min_x,SAW.max_x,SAW.max_x,SAW.min_x]+shift(1),[SAW.min_y,SAW.max_y,SAW.max_y,SAW.min_y,SAW.min_y]+shift(2));
            text(ax,(SAW.min_x+SAW.max_x)/2+shift(1),(SAW.min_y+SAW.max_y)/2+shift(2),SAW.name,'interpreter','none','color',line.Color,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
            hold(ax,'on')
            title(ax,'Structure Preview')
            xlabel(ax,'x (\mu m)')
            ylabel(ax,'y (\mu m)')
            %画内部构件框
            arrayfun(@(x) SAW.structure{x}.show_layout(SAW.layout(x).xy+shift,ax), 1:length(SAW.structure))
        end
        
    end
    methods(Static)
        
    end
end