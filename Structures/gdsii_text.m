classdef gdsii_text<handle
    %The coordinate of canvas center is (0,0)
    properties
        writable_names=["name","group_name","font_height","xy"];
        version=2.0
        %可修改的参数
        name='Default name';
        group_name='USTC-QT-Group';
        font_height=100;
        xy=[0,0];
        %计算的结构参数
        text_value;
        min_x;
        max_x;
        min_y;
        max_y;
        text_width;
        %生成的structure
        layer6=6;
        structure;
        top;
    end
    
    methods
        function obj=gdsii_text(layout)
            %initialization
            for name=string(fieldnames(layout))'
                if any(strcmp(obj.writable_names,name))
                    obj.(name)=layout.(name);
                end
            end
            obj.gen_structure()
        end
        
        function gen_structure(obj)
            % 生成top结构
            %生成文字
            obj.text_value=char(strcat(obj.group_name,'-',obj.name,'-',char(datetime('now','TimeZone','local','Format','y-MM-d'))));
            [cet, obj.text_width]=gdsii_boundarytext(obj.text_value,...
                        [0,0],obj.font_height,0,obj.layer6);
            obj.top=gds_structure(char(strcat(obj.name,'_Text')),cet);
            obj.min_x=-obj.text_width/2;
            obj.max_x=obj.text_width/2;
            obj.min_y=-obj.font_height/2;
            obj.max_y=obj.font_height/2;
        end
        
        function show_layout(obj,shift,ax)
            if nargin<3
                ax=gca;
            end 
            if nargin<2
                shift=0;
            end
            %输入的shift已经包含了向-x方向移动text_width/2，所以要加回来
            line=plot(ax,[obj.min_x,obj.min_x,obj.max_x,obj.max_x,obj.min_x]+obj.text_width/2+shift(1),[obj.min_y,obj.max_y,obj.max_y,obj.min_y,obj.min_y]+shift(2));
            text(ax,(obj.min_x+obj.max_x)/2+obj.text_width/2+shift(1),(obj.min_y+obj.max_y)/2+shift(2),obj.text_value,'interpreter','none','color',line.Color,...
                'HorizontalAlignment','center','VerticalAlignment','middle')
        end
    end
    methods(Static)
        
    end
end