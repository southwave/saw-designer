# SAW-Designer

## 介绍
---
用gdsii-toolbox来画SAW的版图。

## 软件架构
---
- gdsii_chip() --用于生成chip的最顶层程序， **可以** 单独使用
    - gdsii_SAW() --用于生成SAW器件的最顶层程序， **可以** 单独使用
        - gdsii_IDT() --用于生成IDT的子程序， **不能** 单独使用
        - gdsii_mirror() --用于生成mirror的子程序， **不能** 单独使用
        - gdsii_text() --用于生成text的子程序， **不能** 单独使用


## 安装教程
---
### SAW-Designer依赖于gdsii-toolbox，所以需要安装gdsii-toolbox。
[gdsii-toolbox下载链接](https://github.com/ulfgri/gdsii-toolbox)

1. 先下载gdsii-toolbox并解压
2. 把解压好的gdsii-toolbox的文件夹及子文件夹加入到matlab的路径中
3. 确保已经安装了c和c++的编译器，如果没有安装相应的编译器可以下载安装[MinGW64 Compiler](https://sourceforge.net/projects/mingw-w64/files/)
4. 安装完后在matlab命令行窗口输入`!gcc`，如果显示以下信息则说明安装成功，接着再输入`mex -setup`检查c/c++编译器有没有被正确配置
> gcc: 致命错误：没有输入文件
5. 把当前文件夹设置成gdsii-toolbox解压后的根目录，在matlab命令行窗口运行`makemex`编译gdsii-toolbox的c和c++代码

### 安装SAW-Designer

1. 下载SAW-Designer并解压
2. 把解压好的文件夹及子文件夹加入到matlab路径中

## 使用说明
---
1.  SAW_Designer.mlaap为gui主文件，运行即可
